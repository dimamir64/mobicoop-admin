import { mount, createLocalVue } from '@vue/test-utils';
import Vuetify from 'vuetify';
import TextFilter from '@/components/filter/TextFilter.vue';

const localVue = createLocalVue();

describe('components/filter/TextFilter.vue', () => {
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();
  });

  const wrapper = mount(TextFilter, {
    localVue, 
    vuetify,
    propsData: {
      value: 'Dupont',
      label: 'Nom',
      type: 'text'
    }
  });

  test('render TextFilter label', () => {
    expect(wrapper.find('label').text()).toBe('Nom');
  });

  test('render TextFilter value', () => {
    expect(wrapper.find('input').element.value).toBe('Dupont');
  });

  const wrapperNumber = mount(TextFilter, {
    localVue, 
    vuetify,
    propsData: {
      value: '10',
      label: 'Id',
      type: 'number'
    }
  });

  test('render TextFilter value', () => {
    expect(wrapperNumber.find('input').element.value).toBe('10');
  });

});
