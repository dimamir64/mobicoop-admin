import { mount, createLocalVue } from '@vue/test-utils';
import Vuetify from 'vuetify';
import SelectFilter from '@/components/filter/SelectFilter.vue';

const localVue = createLocalVue();

describe('components/filter/SelectFilter.vue', () => {
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();
  });

  const wrapper = mount(SelectFilter, {
    localVue, 
    vuetify,
    propsData: {
      value: '2',
      label: 'Ville',
      list: [
        { text: 'Nancy', value: '1' },
        { text: 'Metz', value: '2' },
        { text: 'Lille', value: '3' },
        { text: 'Paris', value: '4' }
      ]
    },
    // needed by v-select
    created() {
      this.$vuetify.lang = {
        t: () => {},
      };
    }
  });

  test('render SelectFilter label', () => {
    expect(wrapper.find('label').text()).toBe('Ville');
  });

  test('render SelectFilter text', () => {
    expect(wrapper.find('.v-select__selection').text()).toBe('Metz');
  });
});
