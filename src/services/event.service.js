// uncomment if needed
// import RequestService from './request.service';
import { getItems, getItem, postItem, putItem, patchItem, deleteItem, postFile } from './common.service';

class EventService {
  route = 'admin/events';
  imageRoute = 'admin/images';
  getItems(sortBy, sortDesc, page, perPage, filter) {
    return getItems(this.route, sortBy, sortDesc, page, perPage, filter).then( response => Promise.resolve(response), error => Promise.reject(error) );
  };
  getItem(id) {
    return getItem(this.route,id).then( response => {
      // we add a newImage field to track image change
      return Promise.resolve({
        ...response,
        newImage: false
      });
    }).then( response => Promise.resolve(response), error => Promise.reject(error) );
  };
  postItem(item) {
    // useTime is mandatory
    if (item.useTime === undefined || item.useTime === null) {
      item.useTime = false;
    }
    // private is mandatory
    if (item.private === undefined || item.private === null) {
      item.private = false;
    }
    return postItem(this.route, item).then( response => Promise.resolve(response), error => Promise.reject(error) );
  };
  putItem(id,item) {
    return putItem(this.route, id, item).then( response => Promise.resolve(response), error => Promise.reject(error) );
  };
  patchItem(id,item) {
    return patchItem(this.route, id, item).then( response => Promise.resolve(response), error => Promise.reject(error) );
  }
  deleteItem(id) {
    return deleteItem(this.route, id).then( response => Promise.resolve(response), error => Promise.reject(error) );
  }
  postImage(item) {
    return postFile(this.imageRoute, item).then( response => Promise.resolve(response), error => Promise.reject(error) );
  }
}

export default new EventService();