import RequestService from './request.service';
import { getItems } from './common.service';

class AnalyticService {
  route = 'admin/analytics';
  getItems(sortBy, sortDesc, page, perPage, filter) {
    return getItems(this.route, sortBy, sortDesc, page, perPage, filter).then( response => Promise.resolve(response), error => Promise.reject(error) );
  }
  getItem(id, territoryId, communityId, filter=null) {
    let params = {};
    if(filter){
      for (const [key, value] of Object.entries(filter)) {
        if (value != '' && value !== null) {
          params[key] = value;
        }
      }
    }
    
    let darkTheme = 0;
    if(JSON.parse(localStorage.getItem('dark'))){
      darkTheme = 1;
    }

    let request_url = this.route+'/'+id+'?darkTheme='+darkTheme
    if (null !== communityId) {
      request_url += '&communityId='+communityId
    }
    if (null !== territoryId) {
      request_url += '&territoryId='+territoryId
    }

    return RequestService
      .get(request_url, params, 'json')
      .then( response => Promise.resolve(response.data), error => Promise.reject(error) );
  }
}

export default new AnalyticService();