// Mixin dedicated to resources list components
import getEnv from '@/utils/env';
import { debounce, values } from 'lodash';

var ResourceListMixin = {
  data: () => ({
    // selected items in the datatable
    selected: [],
    // footer props
    footerProps: {
      'showFirstLastPage': true,
      'showCurrentPage': true,
      'firstIcon': 'mdi-page-first',
      'lastIcon': 'mdi-page-last',
      'items-per-page-options': [10, 30, 50, 100]
    },
    // breadcrumb init
    breadcrumb: [],
    // resource id (name of the primary key)
    itemId: 'id',
    emptyFilter: true
  }),
  mounted() {
    this.createBreadcrumb();
  },
  computed: {
    // options (sorting, page, items per page) => in the store
    options: {
      get: function() {
        return this.$store.getters[this.resource+'/options'];
      },
      set: function(options) {
        const { sortBy, sortDesc, page, itemsPerPage } = options;
        this.$store.dispatch(this.resource+'/saveOptions', { sortBy, sortDesc, page, itemsPerPage });
      }
    },
    // filter object => in the store
    filter: {
      get: function() {
        return this.$store.getters[this.resource+'/filter'];
      },
      set: function(filter) {
        this.$store.dispatch(this.resource+'/saveFilter', filter);
      }
    },
    // selected optional filters => in the store
    selectedOptionalFilters: {
      get: function() {
        return this.$store.getters[this.resource+'/selectedOptionalFilters'];
      },
      set: function(selectedOptionalFilters) {
        // we need to send the selected optional filters AND the permanent filters 
        // to remove potential optional filtering that is not concerned anymore
        const alwaysOnFilters = this.alwaysOnFilters().map(field => field.name);
        this.$store.dispatch(this.resource+'/saveSelectedOptionalFilters', {selectedOptionalFilters, alwaysOnFilters});
      }
    },
    // items => in the store
    items() {
      return this.$store.getters[this.resource+'/items'];
    },
    loading() {
      return this.$store.getters[this.resource+'/loading'];
    },
    // total number of items (with current filters applied) => in the store
    total() {
      return this.$store.getters[this.resource+'/total'];
    },
    canExport() {
      return getEnv('VUE_APP_EXPORT') == 'true' && this.$store.getters['auth/authorized']('export');
    },
    error: {
      get: function() {
        return this.$store.getters[this.resource+'/error'];
      },
      set: function() {
        this.$store.dispatch(this.resource+'/resetError');
      }
    }
  },
  watch: {
    // what to do when the options (sorting, page, number of items per page) change 
    options: {
      handler () {
        this.getItems();
      },
      deep: true,
    },
    // what to do when the filters change
    filter: {
      handler: debounce(function() {
        this.selected = [];
        this.getItems();
      },500),
      deep:true,
    },
    // an error happened
    error(val) {
      if (val) {
        this.$store.dispatch(this.resource+'/resetError');
      }
    }
  },
  methods: {
    // creation of the breadcrumb from the routes
    createBreadcrumb() {
      this.addToBreadcrumb(this.$route);
      this.$store.commit('SET_BREADCRUMB', this.breadcrumb);
    },
    // recursive function to add a route to the breadcrumb
    addToBreadcrumb(route) {
      if (route.meta.parent) {
        const parentRoute = this.$router.options.routes.find( proute => proute.name === route.meta.parent );
        if (parentRoute !== undefined) {
          this.addToBreadcrumb(parentRoute);
        }
      }
      this.breadcrumb.push(
        { 
          text: this.$t('routes.'+route.name+'.breadcrumb'),
          to: !route.skip ? route.path : null,
          exact: true
        }
      );
    },
    // get the headers of the datatable (columns)
    headers() {
      return this.fields()
        .filter(function(field) {
          if (field.hidden !== undefined) {
            return false;
          }
          return true;
        })
        .map(field => ({
          text: field.label,
          value: field.name,
          sortable: field.sortable !== undefined ? field.sortable : true
        }));
    },
    // get the permanent filters
    alwaysOnFilters() {
      return this.fields()
        .filter(function(field) {
          if (field.filter !== undefined && field.filter.alwaysOn) {
            return true;
          }
          return false;
        })
        .map(field => ({
          label: field.filter.label ? field.filter.label : field.label,
          name: field.filter.name ? field.filter.name : field.name,
          type: field.filter.type,
          componentType: field.filter.componentType ? field.filter.componentType : null,
          cols: field.filter.cols,
          values: field.filter.values,
          value: field.filter.value,
          interval: field.filter.interval,
          resource: field.filter.resource
        }));
    },
    // get the optional filters
    optionalFilters() {
      return this.fields()
        .filter(function(field) {
          if (field.filter !== undefined && !field.filter.alwaysOn) {
            return true;
          }
          return false;
        })
        .map(field => ({
          text: field.label,
          value: field.name+(field.filter.interval ? '['+field.filter.interval+']' : ''),
          type: field.filter.type,
          componentType: field.filter.componentType ? field.filter.componentType : null,
          cols: field.filter.cols,
          values: field.filter.values,
          interval: field.filter.interval,
          resource: field.filter.resource
        }));
    },
    // get the selected optional filters (the ones that are currently displayed)
    displayedOptionalFilters() {
      let selectedFields = this.selectedOptionalFilters;
      return this.fields()
        .filter(function(field) {
          if (selectedFields.includes(field.name+(field.filter ? (field.filter.interval ? '['+field.filter.interval+']' : '') : ''))) {
            return true;
          }
          return false;
        })
        .map(field => ({
          label: field.label,
          name: field.filter.name ? field.filter.name : field.name,
          type: field.filter.type,
          componentType: field.filter.componentType ? field.filter.componentType : null,
          cols: field.filter.cols,
          values: field.filter.values,
          interval: field.filter.interval,
          resource: field.filter.resource
        })); 
    },
    // get the items to populate the datatable
    getItems() {
      this.emptyFilter = values(this.filter).every(this.isEmptyValues);
      this.$store.dispatch(this.resource+'/loadItems');
    },
    // show item action
    showItem (item) {
      return this.$router.options.routes.find( proute => proute.name === this.$route.meta.show ).path.replace(':'+this.itemId,item[this.itemId]);
    },
    // edit item action
    editItem (item) {
      return this.$router.options.routes.find( proute => proute.name === this.$route.meta.edit ).path.replace(':'+this.itemId,item[this.itemId]);
    },
    // add item action
    addItem() {
      return this.$route.meta.create ? this.$router.options.routes.find( proute => proute.name === this.$route.meta.create ).path : null;
    },
    // export all items action
    exportAll() {
      this.$store.dispatch(`${this.resource}/exportAll`);
    },
    // export all items action
    exportFiltered() {
      console.log('export filtered');
    },
    // export selected items action
    exportSelected() {
      console.log('export selected');
    },
    // handler for special component filters (not handled automatically via v-model)
    filterChanged(filter) {
      this.filter = filter;
      this.getItems();
    },
    pushTo(item) {
      this.$router.push(this.showItem(item));
    },
    isEmptyValues(value) {
      return (value === undefined) || (value === null) || (typeof value === 'object' && Object.keys(value).length === 0) || (typeof value === 'string' && value.trim() && value.trim().length === 0);
    }
  }
};

export default ResourceListMixin;