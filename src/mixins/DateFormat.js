// Mixin dedicated to date formating
import moment from 'moment-timezone';

var DateFormatMixin = {
  methods: {
    asSimpleDate(date,utc=false) {
      let momentDate = utc ? moment.utc(date) : moment(date);
      if (momentDate.isValid()) {
        return momentDate.format(this.$t('dates.simpleDate'));
      }
      return null;
    },
    asSimpleDateUS(date,utc=false) {
      let momentDate = utc ? moment.utc(date) : moment(date);
      if (momentDate.isValid()) {
        return momentDate.format(this.$t('dates.simpleDateUS'));
      }
      return null;
    },
    asWeekdayDate(date,locale) {
      moment.locale(locale);
      let momentDate = moment(date);
      if (momentDate.isValid()) {
        return momentDate.format(this.$t('dates.weekdayDate'));
      }
      return null;
    },
    asDateTime(date, utc = false) {
      let momentDate = utc ? moment.utc(date) : moment(date);
      if (momentDate.isValid()) {
        return momentDate.format(this.$t('dates.dateTime'));
      }
      return null;
    },
    asIsoDate(date) {
      let momentDate = moment(date);
      if (momentDate.isValid()) {
        return momentDate.format(this.$t('dates.isoDate'));
      }
      return null;
    },
    asIsoDateTime(date, utc = false) {
      let momentDate = utc ? moment.utc(date) : moment.tz(date,moment.tz.guess());
      if (momentDate.isValid()) {
        return momentDate.format(this.$t('dates.isoDateTime'));
      }
      return null;
    },
    asHourMinute(date,utc=false) {
      let momentDate = utc ? moment.utc(date) : moment(date);
      if (momentDate.isValid()) {
        return momentDate.format(this.$t('dates.hourMinute'));
      }
      return null;
    },
    asHourMinuteSecond(date,utc=false) {
      let momentDate = utc ? moment.utc(date) : moment(date);
      if (momentDate.isValid()) {
        return momentDate.format(this.$t('dates.hourMinuteSecond'));
      }
      return null;
    },
    asMonthName(date,locale,utc=false) {
      moment.locale(locale);
      let momentDate = utc ? moment.utc(date) : moment(date);
      if (momentDate.isValid()) {
        return momentDate.format(this.$t('dates.monthName'));
      }
      return null;
    },
    asMonthNameAndYear(date,locale,utc=false) {
      moment.locale(locale);
      let momentDate = utc ? moment.utc(date) : moment(date);
      if (momentDate.isValid()) {
        return momentDate.format(this.$t('dates.monthNameAndYear'));
      }
      return null;
    },
    asYear4Digits(date,locale,utc=false) {
      moment.locale(locale);
      let momentDate = utc ? moment.utc(date) : moment(date);
      if (momentDate.isValid()) {
        return momentDate.format(this.$t('dates.year4digits'));
      }
      return null;
    },    
    asFromNow(date,locale) {
      moment.locale(locale);
      let momentDate = moment(date);
      if (momentDate.isValid()) {
        return momentDate.fromNow();
      }
      return null;
    },
  }
};

export default DateFormatMixin;