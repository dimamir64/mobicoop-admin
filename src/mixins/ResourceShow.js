// Mixin dedicated to resources show components

var ResourceShowMixin = {
  data: () => ({
    // breadcrumb init
    breadcrumb: [],
    // current tab
    mtab: null,
    // resource id (name of the primary key)
    itemId: 'id',
  }),
  mounted() {
    this.createBreadcrumb();
  },
  created() {
    this.getItem();
  },
  computed: {
    // item => handled in the store, when the item in the store is modified we are warned here
    item() {
      return this.$store.getters[this.resource+'/item'];
    },
    loading() {
      return this.$store.getters[this.resource+'/loading'];
    },
    // get the name of the parent route
    parentRoute() {
      const route = this.$route;
      return this.$router.options.routes.find( proute => proute.name === route.meta.parent );
    },
    // get the path of the parent route
    parentComponentPath() {
      return this.parentRoute ? this.parentRoute.path : null; 
    },
    // get all the possible fields of the current item 
    // we extract the fields from the informations declared in the tabs
    fields() {
      const fields = [];
      const tabs = this.tabs();
      tabs.forEach( tab => {
        tab.rows && tab.rows.forEach( row => {
          row.fields.forEach( field => {
            if (field.name !== 'spacer') {
              fields.push(field.name); 
            }
          });
        });
        if (tab.component && tab.name) {
          fields.push(tab.name);
        }
      });
      return fields;
    },
    error: {
      get: function() {
        return this.$store.getters[this.resource+'/error'];
      },
      set: function() {
        this.$store.dispatch(this.resource+'/resetError');
      }
    }
  },
  watch: {
    // when switching to another item on the same route
    $route() {
      this.getItem();
    },
    // an error happened
    error(val) {
      if (val) {
        this.$store.dispatch(this.resource+'/resetError');
      }
    }
  },
  methods: {
    // creation of the breadcrumb from the routes
    createBreadcrumb() {
      this.addToBreadcrumb(this.$route);
      this.$store.commit('SET_BREADCRUMB', this.breadcrumb);
    },
    // recursive function to add a route to the breadcrumb
    addToBreadcrumb(route) {
      if (route.meta.parent) {
        const parentRoute = this.$router.options.routes.find( proute => proute.name === route.meta.parent );
        if (parentRoute !== undefined) {
          this.addToBreadcrumb(parentRoute);
        }
      }
      this.breadcrumb.push(
        { 
          text: this.$t('routes.'+route.name+'.breadcrumb'),
          to: !route.skip ? route.path : null,
          exact: true
        }
      );
    },
    getSideItemData(name, val) {
      const item = this.side().items.find( sideItem => sideItem.name === name);
      if (item !== undefined && item.values) {
        const data = item.values.find( value => value.value === val);
        if (data !== undefined) return data;
      }
      return {
        icon: '',
        test: ''
      };
    },
    // get the item => we ask the store
    getItem() {
      this.$store.dispatch(this.resource+'/loadItem', { id: this.$route.params.id, fields:this.fields});
    },
    // edit item action
    editItem () {
      return this.$router.options.routes.find( proute => proute.name === this.$route.meta.edit ).path.replace(':'+this.itemId,this.item[this.itemId]);
    },
    // refresh without confirmation
    hardRefresh() {
      this.getItem();
    }
  }
};

export default ResourceShowMixin;