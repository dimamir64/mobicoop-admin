# MobiAdmin

Mobicoop-platform administration using Vuejs stack (Vue + Vue-router + VueX).

## Install
Just run : 
```
npm install
```
to install the needed dependencies.

Eventually, create a `.env.local` file to overload the default `.env` file : there you can specify the url for the mandatory API !

### Compiles and hot-reloads for development (port 8085)
```
npm run serve
```

### Unit test
```
npm run test:unit
```

### Contribution

See [contributing](CONTRIBUTING.md)