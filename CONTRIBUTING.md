# Guide(lines) : how to contribute to Mobicoop platform administration

## 1 - Where to write my code ?

### 1.1 - Vue components

Vue components are grouped in directories, located in [src/components](./src/components). The directories represent the categories of concepts of Mobicoop platform : 
* [src/components/article](./src/components/article) for articles
* [src/components/campaign](./src/components/campaign) for campaigns (mass communication)
* [src/components/community](./src/components/community) for communities
* [src/components/dashboard](./src/components/dashboard) for dashboards (not used anymore as of 09/28/2021, a new dashboard system is planned)
* [src/components/editorial](./src/components/editorial) for editorial articles
* [src/components/event](./src/components/event) for events
* [src/components/relayPoint](./src/components/relayPoint) for relay points
* [src/components/relayPointType](./src/components/relayPointType) for relay point types
* [src/components/solidary](./src/components/solidary) for solidary related components
* [src/components/user](./src/components/user) for users

Create a new directory for a new category, that is not covered yet in the admin.

Usually, each group consists in Edit, Show and List components. **Prefix** these components with the **name of the category**, and use **PascalCase** for components filenames.

Additionally, you will find 2 other directories : 
* [src/components/field](./src/components/field) : contains field-type components that can be used in other components (DateField, ImageField...); feel free to add any useful field component there
* [src/components/filter](./src/components/filter) : contains special field-type components that are used as filters for list components (AutocompleteFilter, SelectFilter...); again, feel free to add any useful filter component there

Finally, you will find the main component called MobiHome here (it's the component that is displayed on the home page).

### 1.2 - Layouts

Layouts are located in [src/layouts](./src/layouts). There are 2 layouts : 
* LoginLayout, for the Login page
* DashboardLayout, for any other page

Use **PascalCase** for layouts filenames.

### 1.3 - Mixins

Mixins are js scripts that are reusable in components. They are located in [src/mixins](./src/mixins). Use **PascalCase** for mixins filenames.

### 1.4 - Models

Classes for internal use objects. Only one model for now : muser (the object used to keep connected user informations).

### 1.5 - Plugins

Vue plugins are located in [src/plugins](./src/plugins). Use **kebab-case**.

### 1.6 -Router 

Router files are used to easily find routes for each category, they are located in [src/router](./src/router). They should all be referenced by the main [src/router/index.js](./src/router/index.js) file. Use **camelCase**.

### 1.7 - Services 

Services are files used to get data, typically from the Mobicoop platform API. They are located in [src/services](./src/services). Most of services use the [common service](./src/services/common.service.js) (and the [request service](./src/services/request.service.js) through the common service). Use **camelCase** and the **.service** suffix.

### 1.8 - Vuex store

Files related with the Vuex store (called _modules_) are located in [src/store](./src/store). They should all be referenced by the main [src/store/index.js](./src/store/index.js) file. Most of store modules use the [common module](./src/store/common.module.js). Use **camelCase** and the **.module** suffix.

### 1.9 - SCSS styles

Scss files are located in [src/styles](./src/styles). If your style is linked to a given component, use the component name with **.scss** extension, in **PascalCase**.

### 1.10 - Translations

Translations are json files located in [src/translations](./src/translations). There's a directory for each type of translation : 
* [src/translations/components](./src/translations/components) : translations for components (UI elements of components)
* [src/translations/layouts](./src/translations/layouts) : translations for layouts 
* [src/translations/messages](./src/translations/messages) : translations for UI messages
* [src/translations/models](./src/translations/models) : translations for models (one directory for each category of concept)
* [src/translations/routes](./src/translations/routes) : translations for routes
* [src/translations/views](./src/translations/views) : translations for views

Each directory itself can contain directories, depending on grouping needs. Use **camelCase** for leaf directories (the ones that finally contains the translations).

Create a json file per language, using its parent directory name followed by an underscore and the 2-letters language code. An index.js should also be present, referencing the json files.

### 1.11 - Views 

Views are master vue components for each category. They are used to display the right component for the given route. Use the plural of the category name, in **camelCase**.

## 2 - Code guidelines

### 2.1 - IDE

**We recommend using VSCode !!!**

Install the ESLint and Vetur extensions. The rules for ESLint can be found in [.eslintrc.js](.eslintrc.js).

### 2.2 - JS and Vue files (\<script\> part)

Variables and functions name must be in **camelCase**. 

### 2.3 - Vue files (\<template\> part)

Use **kebab-case** for html tags and component. 

### 2.4 - Translations

Every single piece of UI must be translated !

### 2.5 - Comments

Use comments only to explain a difficult part of your code, or to make the reading easier for long templates. 

A **good code** should be self-explanatory ! Whereas a _bad code_ is self-explanatory as of a bad code (because it's a bad code).

### 2.6 - CLA

Don't forget to sign the CLA by adding yours in [ContributorLicenseAgreement](ContributorLicenseAgreement).